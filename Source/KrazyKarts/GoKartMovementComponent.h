// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GoKartMovementComponent.generated.h"


USTRUCT()
struct FGoKartMove
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	float Throttle;

	UPROPERTY()
	float Turn;

	UPROPERTY()
	float DeltaTime;

	UPROPERTY()
	float Time;

	bool IsValid() const
	{
		return FMath::Abs(Throttle) <= 1 && FMath::Abs(Turn) <= 1;
	}
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class KRAZYKARTS_API UGoKartMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGoKartMovementComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void SimulateMove(const FGoKartMove Move);

	FVector GetVelocity();
	void SetVelocity(FVector NewVelocity);

	void SetThrottle(float NewThrottle);
	void SetTurn(float NewTurn);

	FGoKartMove GetLastMove() { return LastMove; }

private:
	FGoKartMove CreateMove(float DeltaTime);
		
	void ApplyRotation(float DeltaTime, float Turn);

	void UpdateLocationFromVelocity(float DeltaTime);

	FVector CalculateAirResistance();

	FVector CalculateRollingResitance();

	// The mass of the  car (kg).
	UPROPERTY(EditAnywhere)
	float Mass = 1000.0f;

	// The force applied to the car when the throttle is fully down (N).
	UPROPERTY(EditAnywhere)
	float MaxDrivingForce = 10000.0f;

	// Turning circle radius (m).
	UPROPERTY(EditAnywhere)
	float MinimumTurningRadius = 10.0f;

	// Higher is more drag. kg/m
	UPROPERTY(EditAnywhere)
	float DragCoefficient = 16.0f;

	// The rolling counter coefficint.
	UPROPERTY(EditAnywhere)
	float RollingCoefficient = 0.025f;

	FVector Velocity;

	float Throttle = 0.0f;
	float Turn = 0.0f;

	FGoKartMove LastMove;

};
